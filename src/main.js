const express = require("express");
const app = express();
const port = 3000;
const mylib = require("./mylib");

app.get("/", (req, res) => {
  res.send("Hello world");
});
app.get("/add", (req, res) => {
  const a = parseInt(req.query.a);
  const b = parseInt(req.query.b);
  res.send(String(mylib.sum(a, b)));
});
app.listen(port, () => {
  console.log(`Server: http://localhost:${port}`);
});
